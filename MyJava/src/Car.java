

import java.io.Serializable;

public class Car implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1276048427983569587L;
	private int year;
	private int enginesize;
	private int price;
	
	public Car(int y,int es,int p) {
		this.year=y;
		this.enginesize=es;
		this.price=p;
		
	}
	
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getEnginesize() {
		return enginesize;
	}
	public void setEnginesize(int enginesize) {
		this.enginesize = enginesize;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	

}
