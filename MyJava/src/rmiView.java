
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

public class rmiView extends JFrame{

	private JPanel contentPane;
	private JTextField textYear;
	private JTextField textEngineSize;
	private JTextField textPrice;
	private JButton btnTax;
	private JButton sPrice;
	JTextArea textArea ;
	
	public rmiView() {
		
		setTitle("RMI Swing");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 300, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel insertDetails = new JLabel("Insert new car details");
		insertDetails.setBounds(10, 11, 120, 14);
		contentPane.add(insertDetails);

		JLabel iYear = new JLabel("year:");
		iYear.setBounds(10, 36, 60, 14);
		contentPane.add(iYear);

		JLabel iengineSize = new JLabel("engine size:");
		iengineSize.setBounds(10, 61, 60, 14);
		contentPane.add(iengineSize);

		JLabel iPrice = new JLabel("price");
		iPrice.setBounds(10, 86, 46, 14);
		contentPane.add(iPrice);

		textYear = new JTextField();
		textYear.setBounds(80, 33, 86, 20);
		contentPane.add(textYear);
		textYear.setColumns(10);

		textEngineSize = new JTextField();
		textEngineSize.setBounds(80, 58, 86, 20);
		contentPane.add(textEngineSize);
		textEngineSize.setColumns(10);

		textPrice = new JTextField();
		textPrice.setBounds(80, 83, 86, 20);
		contentPane.add(textPrice);
		textPrice.setColumns(10);

		btnTax = new JButton("tax");
		btnTax.setBounds(10, 132, 89, 23);
		contentPane.add(btnTax);
		
		sPrice = new JButton("price");
		sPrice.setBounds(10, 162, 89, 23);
		contentPane.add(sPrice);
		
		textArea = new JTextArea();
		textArea.setBounds(235, 131, 171, 120);
		contentPane.add(textArea);
		
	}
	
	
	public void addBtnTaxActionListener(ActionListener e) {
		btnTax.addActionListener(e);
	}

	public void addBtnPriceActionListener(ActionListener e) {
		sPrice.addActionListener(e);
	}
	

	public String getCarYear() {
		return textYear.getText();
	}
	
	public String getCarEngineSize() {
		return textEngineSize.getText();
	}

	public String getCarPrice() {
		return textPrice.getText();
	}
	
	public void writeToTA(String s) {
		textArea.setText(s);
	}

	
}
