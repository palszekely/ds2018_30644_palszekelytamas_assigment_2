
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CarOpInf extends Remote {
   public double taxCar(Car c) throws RemoteException;
    public double priceCar(Car c) throws RemoteException;
}
