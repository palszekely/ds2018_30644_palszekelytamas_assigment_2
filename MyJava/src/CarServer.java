

import java.rmi.Naming;

import java.rmi.RemoteException; 
import java.rmi.server.UnicastRemoteObject;
import java.rmi.AlreadyBoundException;


public class CarServer extends UnicastRemoteObject implements CarOpInf  { 
   public CarServer() throws RemoteException {} 
   
   
   
   private static final long serialVersionUID = 1151719468375935127L;


	


	public double priceCar(Car c) throws RemoteException {
		// Dummy formula
		if ((2018-c.getYear()) >= 7) {
			return 0;
		}
		return (c.getPrice()-c.getPrice()/7*(2018-c.getYear()));

	}
	
	
	public double taxCar(Car c) throws RemoteException {
		// Dummy formula
		if (c.getEnginesize() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		int sum = 8;
		if(c.getEnginesize() > 1601) sum = 18;
		if(c.getEnginesize() > 2001) sum = 72;
		if(c.getEnginesize() > 2601) sum = 144;
		if(c.getEnginesize() > 3001) sum = 290;
		return c.getEnginesize() / 200.0 * sum;
	}
	
	 
   
   
   
   public static void main(String args[]) throws RemoteException,AlreadyBoundException { 
	   /*
      try { 
    	  
         // Instantiating the implementation class 
    	  System.setSecurityManager(new RMISecurityManager());
         CarOp obj = new CarOp(); 
         //String url="rmi://127.0.0.1:1099/exem";  
         System.setProperty("java.rmi.server.hostname","localhost");
         Naming.rebind("example", obj); 
         System.err.println("Server ready"); 
         
      } catch (Exception e) { 
    	  
         System.err.println("Server exception: " + e.toString()); 
         e.printStackTrace(); 
         
      } 
 */
	   try {
 
           
           // create a new service named myMessage
           Naming.rebind("//localhost/myMessage", new CarServer());  
	   }
	   catch(Exception e) {
		   e.printStackTrace();
	   }
   } 
   
}