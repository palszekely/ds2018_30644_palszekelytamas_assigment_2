
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.rmi.Naming;

public class rmiController {
	
	private rmiView rmiView;


	public rmiController() {
		rmiView = new rmiView();
		rmiView.setVisible(true);


		rmiView.addBtnTaxActionListener(new GetActionListener());
		rmiView.addBtnPriceActionListener(new Get1ActionListener());
	}

	class GetActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String myyear = rmiView.getCarYear();
			String mycarengine = rmiView.getCarEngineSize();
			String myprice = rmiView.getCarPrice();
			int my=Integer.parseInt(myyear);
			int mce=Integer.parseInt(mycarengine);
			int mp=Integer.parseInt(myprice);
            Car c=new Car(my,mce,mp);
            try{  
	    		  
    	    	
	              
	              // search for myMessage service
	    		 
	              CarOpInf impl = (CarOpInf) Naming.lookup("//localhost/myMessage");
	               
	              // call server's method        
	              rmiView.writeToTA(String.valueOf(impl.taxCar(c)));
	        
	    		 
	    		  
	      } catch (Exception er) {
	         System.err.println("Client exception: " + e.toString()); 
	         er.printStackTrace(); 
	      } 
			

				 
			
		}
	}
	
	

	class Get1ActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String myyear = rmiView.getCarYear();
			String myprice = rmiView.getCarPrice();

			int my=Integer.parseInt(myyear);
			int mp=Integer.parseInt(myprice);
			 Car c=new Car(my,0,mp);
				
			try{  
	    		  
    	    	
	              
	              // search for myMessage service
	    		 
	              CarOpInf impl = (CarOpInf) Naming.lookup("//localhost/myMessage");
	               
	              // call server's method        
	              rmiView.writeToTA(String.valueOf(impl.priceCar(c)));
	           
	    		 
	    		  
	      } catch (Exception er) {
	         System.err.println("Client exception: " + e.toString()); 
	         er.printStackTrace(); 
	      } 
			
		}
	}
	
}